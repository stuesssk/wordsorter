#define _XOPEN_SOURCE

#include "sort.h"

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>

// need to handle up to supercalfragalisticexpialidocious
enum{MAX_WORD_LEN = 48};
// Max line size
enum{LINE_BUFF = 512};

int main(int argc, char *argv[])
{

    int firstAmount = 0;  // used when user only wants first n in list
    bool reverse = false;
    bool unique = false;
    int lastSort = 0; // used to determine which sort used

    // Suppress getopt default error handling
    opterr = 0;

    // Lets get the option flags
    int c;
    while (-1 < (c = getopt(argc, argv, "c:rnlsauh"))){
        char *errGetOpt;
        switch (c){
            case 'c':
                // if multiple -u this will only store the last one
                firstAmount = strtol(optarg, &errGetOpt, 10);
                if (*errGetOpt){
                    printf("Error: first_n must be number");
                    return EX_USAGE;
                }
                break;
            case 'r':
                // Handling multiple -r flags
                reverse = !reverse;
                break;
            case 'n':
                lastSort = 1;
                break;
            case 'l':
                lastSort = 2;
                break;
            case 's':
                lastSort = 3;
                break;
            case 'a':
                lastSort = 4;
                break;
            case 'u':
                // mulitple -u will not cancel out
                unique = true;
                break;
            case 'h':
                printf("\nSorts words in files given in command "
                       "line of from stdin if no files are given.\n"
                       "The different flags change which manner the words"
                       " are sorted\nProper syntax:\n"
                       "./ws [-c <number>][-r][-n][-l][-s][-a][-u]"
                       "[-h][file1][file2]...\n\n");
                exit(0); // Asked for help exiting program after helping
            case '?':
                printf("Unknown option\n");
                printf("./ws [-c <number>][-r][-n][-l][-s][-a][-u]");
                printf("[-h][file1][file2]...\n");
                return EX_USAGE;
            default:
                printf("Default option handler got '%c'\n", c);
                return EX_USAGE;
        }
    }

    size_t index = 0;
    char **wordList = malloc((1+ index) * sizeof(*wordList));
    if (!wordList) {
        return EX_OSERR;
    }
    wordList[0] = NULL;

    char *token;
    char line[LINE_BUFF];
    char word[MAX_WORD_LEN];
    // files start at optind value if they exist
    int i = optind;
    if (optind < argc){
        while (i < argc){
            FILE *fp = fopen(argv[i], "r");
            if (!fp){
                printf("Unable to open %s for reading", argv[i]);
                return 0; 
            }
            // storing in list so long as there are words on line
            while (fgets(line, sizeof(word), fp)){
                token = strtok(line, " \t\n");
                while (token){

                    ++index;

                    void *tmp = realloc(wordList, (1 + index) * sizeof(*wordList));
                    if (tmp){
                        wordList = tmp;
                    } else {
                        break;
                    }

                    char buf[MAX_WORD_LEN];
                    size_t sz = sizeof(buf) + 1;

                    char *words = malloc(sz);
                    if (!words){
                        return EX_OSERR;
                    }

                    strncpy(words, token, sz);

                    wordList[index - 1] = words;
                    wordList[index] = NULL;

                    // This token evaluated at start of loop
                    // NULL we are done, else store a nother word
                    token = strtok(NULL, " \t\n");
                }
            }
            // DO we live in a barn, close that file!
            if ((fclose(fp))){
                printf("Unable to close %s for reading\n", argv[i]);
            }
            ++i;
        }
    } else {
        printf("Enter your words:");
        fgets(line, sizeof(word), stdin);
        char *token = strtok(line, " \t\n");
        while (token){

            ++index;

            void *tmp = realloc(wordList, (1 + index) * sizeof(*wordList));
            // If unable to malloc bail
            if (tmp){
                wordList = tmp;
            } else {
                printf("Unable to malloc!?");
                break;
            }

            // Mallocing space for the word
            char buf[MAX_WORD_LEN];
            size_t sz = sizeof(buf) + 1;
            char *words = malloc(sz);
            if (!words){
                printf("Unable to malloc!?");
                return EX_OSERR;
            }

            strncpy(words, token, sz);
            // All that work and I can finally store the word
            wordList[index - 1] = words;
            // Ensure a NULL byte is tacked on the end of the array
            wordList[index] = NULL;
            token = strtok(NULL, " \t\n");
        }
    }
    // Determine the type of sort
    // sorting first lixigraphiclly handles when other sorts have equal values
    // i.e. dog cat will be sorted to cat dog for length sort.
    qsort(wordList, index, sizeof(*wordList), cmpWords);
    switch (lastSort){
        case 1:
            qsort(wordList, index, sizeof(*wordList), sortnumerical);
            break;
        case 2:
            qsort(wordList, index, sizeof(*wordList), cmpWordLen);
            break;
        case 3:
            qsort(wordList, index, sizeof(*wordList), sortScrabble);
        default:;
    }
    // If printing first N has not been specified by getopt
    // be sure to print ALL the words
    if (firstAmount == 0){
        firstAmount = index;
    }

    // Give it to them in reverse
    if (reverse){
        qsort(wordList, index, sizeof(*wordList), revcmpWords);
    }

    // Temporary array of pointers to handle printing and 
    // to ensure all mallocs get freed
    char **finalList = wordList;
    if (unique){
        uniqueWords(wordList, index);
    } else {
        // printing regularly if unique flag is not set
        // otherwise uniqueWords function handles printing unique words
        for (i = 0; i < firstAmount; ++i){
            if (lastSort == 3){ // 3 is lastSort flag for Scabble® score
                printf("Scrabble pts:%3d ", scrabblescore((const void*)finalList));
            }
            printf("%s\n", *finalList);
            ++finalList;
        }
    }

    // finalList is currently pointing to the end of the list
    // subtract index to get to beginning
    finalList -= index;
    // The Valgrind overlords proclaim to free the mallocs
    while ( *finalList){
        free(*finalList);
        ++finalList;
    }
    free(wordList); // Never leave a malloc unfreed
}
