#ifndef SORT_H
#define SORT_H

#include <stdio.h>

int sort(char **sort, size_t length);

int cmpWords(const void *a, const void *b);

int cmpWordLen(const void *a, const void *b);

int revcmpWords(const void *a, const void *b);

void uniqueWords(char **list , int length);

int sortScrabble(const void *a, const void *b);

int sortnumerical(const void *a, const void *b);

int scrabblescore(const void *a);

#endif
