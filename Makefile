CPPFLAGS+=-Wall -Wextra -Wpedantic
CPPFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal
CPPFLAGS+=-Waggregate-return -Winline

CFLAGS+=-std=c11

BIN=ws
OBJS=ws.o sort.o

.PHONY: clean debug profile

$(BIN): $(OBJS)

debug: CFLAGS+=-g
debug: $(BIN)

# Profiling requires flags for both compiler and linker
profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: $(BIN)

clean:
	$(RM) $(OBJS) $(BIN)
