#include "sort.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int cmpWords(const void *a, const void *b)
{
    // Comparison taken from class intsruction of LIAM ECHLIN
    if (!a){
        // Cast b to an int based on it being NULL or not
        return !!b;
    } else if (!b){
        return -1;
    }
    // Dereferenceing strings in this was manner taken from
    // source: https://bewuethr.github.io/sorting-strings-in-c-with-qsort
    return strcmp(*(const char **)a, *(const char **)b);
}

int cmpWordLen(const void *a, const void *b)
{
    // Comparison taken from class intsruction of LIAM ECHLIN
    if (!a){
        // Cast b to an int based on it being NULL or not
        return !!b;
    } else if(!b){
        return -1;
    }
    // Dereferenceing strings in this manner was taken from
    // source: https://bewuethr.github.io/sorting-strings-in-c-with-qsort
    return strlen(*(const char **)a) - strlen(*(const char **)b);
}

int revcmpWords(const void *a, const void *b)
{
    // Comparison taken from class intsruction of LIAM ECHLIN
    if (!a){
        // Cast b to an int based on it being NULL or not
        return !!b;
    } else if(!b){
        return -1;
    }
    // memory address increase as you parse through the array. position 1
    // has a lower address than position 2.
    return (&a - &b);
}

void uniqueWords(char **list , int length)
{
    // scomparing each word to all the words in the list after it
    for (int i = 0; i < length; ++i){
        for (int j = (i+1); j < length; ++j){
            if (strcmp(list[i], list[j]) == 0){
                break;
            }
            // if i == j you are at the end of the loop and the word is
            // unique, print it and move on
            if (i == j){
                printf("%s\n", list[i]);
            }
        }
    }
}

int sortScrabble(const void *a, const void *b)
{
    // Comparison taken from class intsruction of LIAM ECHLIN
    if (!a){
        // Cast b to an int based on it being NULL or not
        return !!b;
    } else if(!b){
        return -1;
    }

    int scorea = 0;
    int scoreb = 0;

    scorea = scrabblescore(a);
    scoreb = scrabblescore(b);

    return scorea - scoreb;
}

int sortnumerical(const void *a, const void *b)
{
    // Comparison taken from class intsruction of LIAM ECHLIN
    if (!a){
        // Cast b to an int based on it being NULL or not
        return !!b;
    } else if(!b){
        return -1;
    }
    // Overall layout of sorting numerically accompilshed with aid of
    // Alexander Dow.

    // Using strtol to determine if a or b are strings or numbers.
    // because of this changing NULL to capture first invlid char
    // would not matter since strtol already returns 0 if string.
    int first = strtol(*(char **)a, NULL, 10);
    int second = strtol(*(char **)b, NULL, 10);

    // Zeros at top then words in lexigraphical order, with number
    // on tha tail in numerical order. In the manner that sort -n
    // deals with sorting numerically.
    if (!first){
        if (!second){
            return cmpWords(a, b);
        } else {
            return -1;
        }
    }else if (!second){
        return 1;
    }
    // Makes through the gauntlet they are numbers
    return first - second;
}

int scrabblescore(const void *a)
{
    // If a == NULL return 0 since there are no letters
    if (!a){
        return 0;
    }

    const char **first = (const char **)a;

    int i = 0;
    int scorea = 0;
    char charA;

    int letterValue[26] = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8,
                           5, 1, 3, 1, 1, 3, 10, 1, 1, 1,
                           1, 4, 4, 8, 4, 10};
    // score for a
    // converting to lowercase and subtracting ascii value
    // i.e a-97 = 0 and in letterValue translates to 1 point
    while ((*first)[i]){
        charA = tolower((*first)[i]);
        if (isalpha(charA) != 0){
            scorea += letterValue[charA-97];
        }
        ++i;
    }
    return scorea;
}
